#!/bin/sh

# from https://willhaley.com/blog/custom-debian-live-environment/

sudo debootstrap \
    --arch=i386 \
    --variant=minbase \
    sid \
    $PWD/chroot \
    http://ftp.fr.debian.org/debian/

sudo chroot chroot

sudo apt install linux-image-686-pae live-boot systemd-sysv --no-install-recommends 

