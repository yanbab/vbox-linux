#!/bin/sh
#
# Debian with GNOME 
#
# Setup script
#

#REPO='ftp.debian.org'
REPO='ftp.fr.debian.org'
FLATPAK=false
LATEST=true


_ () {
	echo ""; printf "\033[0;33m$1\033[0m"; echo ""
}

install () {
	sudo apt install -y --no-install-recommends $*
}

write () {
    echo $2 | sudo tee $1
}

hide () {
    echo "Hide $1"
    cp /usr/share/applications/$1 ~/.local/share/applications/
    echo "NoDisplay=true" >> ~/.local/share/applications/$1
}

_ "Update packages"

sudo apt update && sudo apt upgrade -y
	
_ "Upgrade system to Debian SID"

write /etc/apt/sources.list "deb http://$REPO/debian/ sid main contrib non-free"
sudo apt update && sudo apt upgrade -y

_ "Install minimal GNOME Shell"

install gnome-shell gnome-session gdm3 gnome-control-center gkbd-capplet pulseaudio chrome-gnome-shell

_ "Install minimal Xorg server"

install xserver-xorg xserver-xorg-video-all xserver-xorg-input-all

_ "Install GNOME Shell extensions"

install gnome-shell-extension-prefs \
    gnome-shell-extension-dashtodock \
    gnome-shell-extension-no-annoyance \
    gnome-shell-extension-appindicator \
    gnome-shell-extension-desktop-icons
    
_ "Install minimal GNOME Apps"

install gnome-terminal \
	gnome-system-monitor \
	gnome-calculator \
	eog evince \
	gnome-software fwupd \
	gnome-shell-extensions gnome-tweaks \
	gedit gedit-plugin-bracket-completion  gedit-plugin-terminal \
	nautilus nautilus-extension-gnome-terminal nautilus-admin \
	nautilus-share \
	samba
		
_ "Install Web Browser"

	install firefox
	#install chromium-browser
	#install epiphany-browser  

_ "Setup NetworkManager"

	install network-manager
	sudo sed -i 's/managed=false/managed=true/g' /etc/NetworkManager/NetworkManager.conf
	

_ "Setup Plymouth boot animation"

install plymouth plymouth-themes
sudo plymouth-set-default-theme -R spinner
#sudo plymouth-set-default-theme -R futureprototype # debian default

_ "Setup GRUB boot menu"

write /etc/default/grub.d/silent-grub.cfg "GRUB_TIMEOUT=0
GRUB_TERMINAL=console
GRUB_CMDLINE_LINUX_DEFAULT=\"quiet splash\"" 
sudo update-grub

_ "Setup Virtualbox Guest additions"

install virtualbox-guest-x11

_ "Setup system permissions for user $USER"

sudo adduser $USER vboxsf
sudo adduser $USER sambashare

_ "Setup GNOME Shell for user $USER"

gsettings set org.gnome.desktop.privacy remember-app-usage false

_ "Setup nautilus file manager for user $USER"

gsettings set org.gnome.nautilus.preferences confirm-trash      false
gsettings set org.gnome.nautilus.preferences click-policy       'single'
gsettings set org.gnome.nautilus.icon-view   default-zoom-level 'standard'
gsettings set org.gnome.nautilus.list-view   default-zoom-level 'small'
gsettings set org.gnome.nautilus.list-view   use-tree-view      true

_ "Setup Gedit text editor for user $USER"

gsettings set org.gnome.gedit.preferences.ui side-panel-visible false
gsettings set org.gnome.gedit.preferences.ui statusbar-visible  false
gsettings set org.gnome.gedit.preferences.editor tabs-size      4
gsettings set org.gnome.gedit.preferences.editor insert-spaces  true
gsettings set org.gnome.gedit.preferences.editor background-pattern 'grid'
gsettings set org.gnome.gedit.plugins active-plugins "['filebrowser', 'modelines', 'externaltools', 'quickopen', 'quickhighlight', 'terminal']"

_ "Setup Terminal for user $USER"

gsettings set org.gnome.Terminal.Legacy.Settings theme-variant 'dark'

_ "Disable Gnome Software autostart"

mkdir -pv ~/.config/autostart && cp /etc/xdg/autostart/gnome-software-service.desktop ~/.config/autostart/
echo "X-GNOME-Autostart-enabled=false" >> ~/.config/autostart/gnome-software-service.desktop
dconf write /org/gnome/desktop/search-providers/disabled "['org.gnome.Software.desktop']"

_ "Hide useless apps"

hide im-config.desktop
hide software-properties-gnome.desktop


_ "Clean up files"

sudo apt clean
sudo apt --purge autoremove -y

# Quit
_ "  
  Your system is ready ! 
  Please type 'sudo reboot' to finish installation.
  "
