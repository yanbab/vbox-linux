# VBox Linux

![screenshot](https://www.gnome.org/wp-content/uploads/2017/04/featured-image@2x-1.png)

## About

VBox Linux is based on the latest Debian Sid linux distribution and GNOME desktop. As such it's the perfect combination between a rock-solid Debian system, and always up-to-date applications :

- [x] Linux kernel 5.3.0
- [x] GNOME desktop 3.24.1
- [x] Firefox web browser ESR 68.2.0

VBox Linux is fast and small in size, providing a minimal set of applications to get you running : web browser, file browser, terminal emulator, text editor, and *Software* - a great app store to easily find whatever other application you will need.

